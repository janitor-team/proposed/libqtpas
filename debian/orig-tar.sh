#!/bin/sh -xe

PACKAGE_NAME=libqtpas
TMP_DIR=`/bin/mktemp -d -t ${PACKAGE_NAME}.XXXXXX` || exit 1
ORIG_PATH=$(pwd)

while test $# -gt 0
do
	case $1 in
		--upstream-version)
			shift
			VERSION=$1
			;;
		*)
			;;
	esac
	shift
done

ORIG_SRC_DIR=lazarus/lcl/interfaces/qt5/cbindings
DEB_SRC_DIR=${PACKAGE_NAME}-${VERSION}
DEB_SRC_TAR=${PACKAGE_NAME}_${VERSION}.orig.tar.xz
ORIG_SRC_TAR=$(readlink -m ../${DEB_SRC_TAR})

cd ${TMP_DIR}
tar -axf ${ORIG_SRC_TAR} ${ORIG_SRC_DIR}
mv ${ORIG_SRC_DIR} ${DEB_SRC_DIR}
cd ${DEB_SRC_DIR}
find '(' -name '*.icns' -or -name '*.java' ')' -exec chmod a-x {} ';'
find -empty -delete
cd ..
tar -acf ${DEB_SRC_TAR} ${DEB_SRC_DIR}
cd ${ORIG_PATH}
mv ${TMP_DIR}/${DEB_SRC_TAR} ../
rm -rf ${TMP_DIR}
